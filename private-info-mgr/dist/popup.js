document.addEventListener("DOMContentLoaded", function () {
  // 发送消息给content script，请求当前网站的存储信息
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(tabs[0].id, { action: "getAllStorage" }, function (response) {
      // 处理收到的存储信息
      if (response) {
        displayStorage("cookieList", response.cookies);
        displayStorage("localStorageList", response.localStorage);
        displayStorage("sessionStorageList", response.sessionStorage);
      }
    });
  });
});

function displayStorage(listId, storageData) {
  var list = document.getElementById(listId);
  list.innerHTML = ""; // 清空列表

  // 显示每一项存储信息
  storageData.forEach(function (item) {
    var listItem = document.createElement("li");
    listItem.textContent = item;

    var deleteButton = document.createElement("button");
    deleteButton.textContent = "删除";
    deleteButton.addEventListener("click", function () {
      // 发送消息给content script，请求删除存储信息
      chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, { action: "deleteStorage", key: item });
        // 重新加载列表
        chrome.tabs.sendMessage(tabs[0].id, { action: "getAllStorage" }, function (response) {
          if (response) {
            displayStorage(listId, response[listId.replace("List", "")]);
          }
        });
      });
    });

    listItem.appendChild(deleteButton);
    list.appendChild(listItem);
  });
}
