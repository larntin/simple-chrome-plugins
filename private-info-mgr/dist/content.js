chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
  if (request.action === "getAllStorage") {
    // 获取所有存储信息
    var cookies = document.cookie.split(";").map(function (cookie) {
      return cookie.trim();
    });

    chrome.cookies.getAll({ url: "https://www.yuque.com" }, function (cookies) {
      console.log(cookies);
    });

    var localStorageData = [];
    for (var i = 0; i < localStorage.length; i++) {
      var key = localStorage.key(i);
      var value = localStorage.getItem(key);
      localStorageData.push({ key, value });
    }

    var sessionStorageData = [];
    for (var j = 0; j < sessionStorage.length; j++) {
      var keyS = sessionStorage.key(j);
      var valueS = sessionStorage.getItem(keyS);
      sessionStorageData.push({ key: keyS, value: valueS });
    }

    sendResponse({
      cookies: cookies,
      localStorage: localStorageData,
      sessionStorage: sessionStorageData,
    });
  } else if (request.action === "deleteCookie") {
    if (request.key) {
      var _cookiesArray = document.cookie.split(";").map(function (cookie) {
        return cookie.trim();
      });
      var i = _cookiesArray.indexOf(key);
      if (i != -1) _cookiesArray.splice(i, 1);
      document.cookie = _cookiesArray.join(";");
    }
  } else if (request.action === "deleteLocalStorage") {
    if (request.key) {
      localStorage.removeItem(request.key);
    }
  } else if (request.action === "deleteSessionStorage") {
    if (request.key) {
      sessionStorage.removeItem(request.key);
    }
  } else if (request.action === "clearCookie") {
    document.cookie = "";
  } else if (request.action === "clearLocalStorage") {
    localStorage.clear();
  } else if (request.action === "clearSessionStorage") {
    sessionStorage.clear();
  }
});
