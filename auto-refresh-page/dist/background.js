const defaultRefreshTime = 4 * 60 * 1000; //4 分钟刷一次

// 配置文件格式
// config: {
//   urls: [],
//   refreshTime: 240000
// }

chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
  chrome.storage.sync.get(["config"], function ({ config: { urls, refreshTime } }) {
    if (!refreshTime) refreshTime = defaultRefreshTime;
    if (urls && urls.length > 0 && changeInfo.status === "complete") {
      urls.forEach((url) => {
        if (new RegExp(url).test(tab.url)) {
          setTimeout(function () {
            chrome.tabs.reload(tabId);
          }, refreshTime);
        }
      });
    }
  });
});
