window.onload = function () {
  function saveRegex() {
    const regexInput = document.getElementById("regexInput").value;
    chrome.storage.sync.set({ regex: regexInput }, function () {
      console.log("Regex saved:", regexInput);
    });
  }

  document.getElementById("saveBtn").addEventListener("click", saveRegex);
};
