import { defineConfig, loadEnv } from "vite";
import react from "@vitejs/plugin-react";
import { resolve } from "path";

export default defineConfig((command, mode) => {
  const env = loadEnv(mode, process.cwd(), "");

  return {
    base: "./",

    plugins: [react()],

    build: {
      // 多页面应用模式
      rollupOptions: {
        input: {
          options: resolve(__dirname, "options.html"),
          popup: resolve(__dirname, "popup.html"),
        },
      },
      // 将 sourcemap 打包到结果之后去，在在chrome插件面板调试
      sourcemap: "inline",
      // 监听改动自动编译成结果
      watch: {
        buildDelay: 500,
        clearScreen: false,
        include: resolve(__dirname, "./public/background.js"),
      },
    },

    css: {
      preprocessorOptions: {
        // 为 Antd@^4.24.10 开启 less 的配置项
        less: {
          // 支持内联 JavaScript
          javascriptEnabled: true,
          // 重写 less 变量，定制样式
          modifyVars: {
            // 这里修改颜色的优先级最高
            // "@primary-color": "red",
          },
        },
      },

      // 配置 CSS modules 的行为。选项将被传递给 postcss-modules。
      // 源码库及配置：https://github.com/madyankin/postcss-modules
      modules: {
        // generateScopedName详细说明：https://github.com/webpack/loader-utils#interpolatename
        // 就是webpack 下的 loader-utils 工具的 interpolatename参数的文档
        generateScopedName: "[local]__[hash:base64:5]",
        // 加上 [name] 的前缀太长了
        // generateScopedName: "[name]--[local]__[hash:base64:5]",
        hashPrefix: "prefix",
      },
    },

    server: {
      port: env.PORT,
      proxy: {},
    },
  };
});
